import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';  
import { Employee } from '../models/employee.model';


// const httpOptions = {
//   headers: new HttpHeaders({ 'Content-Type': 'application/json',
//   'Access-Control-Allow-Headers': 'Content-Type',
//   'Access-Control-Allow-Methods': 'GET',
//   'Access-Control-Allow-Origin': '*'
//  })
// };
 
        const headers = new HttpHeaders();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        // headers: {'Content-Type', 'multipart/form-data'}
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'GET');
        headers.append('Access-Control-Allow-Origin', '*');


@Injectable()
export class EmployeeService {

  constructor(private http:HttpClient) {}

  // private employeeUrl = 'http://localhost:8080/api/v1/employees';
  private employeeUrl = 'http://localhost:80/one/index.php/api';
  

  public getEmployees() { 

    return this.http.get<Employee[]>(this.employeeUrl + "/item",  {headers:headers});
  }

  public getEmployee(id) {
    return this.http.get(this.employeeUrl + "/get-employee/"+ id,  {headers:headers} );
  }

  public deleteEmployee(employee) {
    return this.http.delete(this.employeeUrl + "/delete-employee/"+ employee.id);
  }

  public createEmployee(employee) {
    return this.http.post<Employee>(this.employeeUrl + "/item/index_post", employee);
  }

  public updateEmployee(employee) {
    return this.http.put<Employee>(this.employeeUrl + "/update-employee", employee);
  }

}
